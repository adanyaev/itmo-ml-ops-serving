from feast import Entity, Feature, FeatureView, FileSource, ValueType
from google.protobuf.duration_pb2 import Duration

driver = Entity(
    name="driver_id", value_type=ValueType.INT64, description="driver id"
)

driver_stats_source = FileSource(
    path="data/driver_stats.parquet",
    event_timestamp_column="event_timestamp",
    created_timestamp_column="created",
)

driver_stats_fv = FeatureView(
    name="driver_stats",
    entities=["driver_id"],
    ttl=Duration(seconds=86400 * 1),
    features=[
        Feature(name="conv_rate", dtype=ValueType.FLOAT),
        Feature(name="acc_rate", dtype=ValueType.FLOAT),
        Feature(name="avg_daily_trips", dtype=ValueType.INT64),
    ],
    online=True,
    input=driver_stats_source,
    tags={},
)
