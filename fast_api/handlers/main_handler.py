from PIL import Image
import numpy as np
import io
from .draw_handler import draw_on_image
from .model_class import LP_detector


async def get_image_after_treatment(file, model_uri: str):
    image = Image.open(io.BytesIO(await file.read())).convert("RGB")
    image_np = np.array(image)
    result_img = image_np.copy()
    lp_detector = LP_detector(model_uri)

    lp_result = lp_detector.inference(result_img)
    lp_boxes, _, _ = (
        lp_result["bboxes"],
        lp_result["scores"],
        lp_result["labels"],
    )
    try:
        [_[0] for _ in lp_boxes]
        result_img = draw_on_image(result_img, lp_boxes)
    except Exception as e:
        print(e)

    returned_image = Image.fromarray(result_img)
    bytes_io = io.BytesIO()
    returned_image.save(bytes_io, format="PNG")
    return_value = bytes_io.getvalue()
    return return_value
