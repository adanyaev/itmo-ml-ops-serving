from .drawing import (
    draw_all_on_image,
)


def draw_on_image(image_np, bboxes):
    result_img = draw_all_on_image(image_np, bboxes)
    return result_img
