from fastapi import FastAPI, File, UploadFile
from fastapi.responses import FileResponse

import os

from starlette.responses import Response

from prometheus_fastapi_instrumentator import Instrumentator

from handlers.main_handler import get_image_after_treatment

app = FastAPI()

Instrumentator().instrument(app).expose(app)


@app.get("/")
async def read_root():
    """Displays greetings"""
    return {"Greetings": "Welcome to our LPR"}


@app.post("/upload_image/", response_class=FileResponse)
async def upload_image(file: UploadFile = File(...)):
    """Posts image file
    Performs image processing
    Returns and displays the image after processing"""
    os_model_uri = os.environ.get("MODEL_URI")
    return Response(
        await get_image_after_treatment(file, model_uri=os_model_uri),
        media_type="image/png",
    )
